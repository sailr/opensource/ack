package com.sailr.opensource.ack

data class Greeting(val id: Long, val content: String)