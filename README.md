# ack

Acknowledge: A Serverless Handler for Everything Between Dev and Ops

## Problem Statement

Ack comes from our experience writing and rewriting CI/CD pipelines. Every organization I've been at, I've rewritten the same 10 steps over and over. Ack is meant to solve this problem by implementing those steps in a generic fashion whilst adding compute and full access to running instances for debugging.

## Overview

Ack's implementation centers around an ack.yml. This ack.yml file acts a DSL to the underlying functionality. Ack also comes with a CLI tool for easy execution from the command line. Each line of the ack.yml file reads as a shell script. The start of the build output will list environment variables, added functionality, and basic runtime environment information. Users can specify their own runtime environment where they deem appropriate.

In addition to the runtime environments, users will be able to setup notifications to various systems. We accomplish this through the use of Jira's API,

## Getting Start

1. Check for or Install Kind
2. Check for or create a new kind cluster
3. Install kubeless into cluster
4. Load in kubeless functions


```bash
./scripts/startup
```