FROM gradle:6.1.1-jdk8 as builder

WORKDIR /workspace

COPY . .

RUN ./gradlew build

FROM gcr.io/distroless/java:11

COPY --from=builder /workspace/build/libs/ack-*.jar ack.jar

USER nonroot:nonroot

EXPOSE 8080

CMD ["/ack.jar"]
